# Ender-3 Pro Cura Settings

## Start G-code

```
; Ender 3 Pro w/ BLTouch Start G-code
G92 E0 ; Reset Extruder
G28 ; Home all axes
G1 Z10.0 F3000 ; Move Z Axis up little to prevent scratching of Heat Bed
G1 X0 Y0

M104 S{material_print_temperature_layer_0}
M190 S{material_bed_temperature_layer_0}
M109 S{material_print_temperature_layer_0}

G1 X0.1 Y20 Z0.3 F5000.0 ; Move to start position
G1 X0.1 Y200.0 Z0.3 F1500.0 E15 ; Draw the first line
G1 X0.4 Y200.0 Z0.3 F5000.0 ; Move to side a little
G1 X0.4 Y20 Z0.3 F1500.0 E30 ; Draw the second line
G92 E0 ; Reset Extruder
G1 Z2.0 F3000 ; Move Z Axis up little to prevent scratching of Heat Bed
G1 X5 Y20 Z0.3 F5000.0 ; Move over to prevent blob squish
M420 S1 ; Recall last bed level
```

## End G-code

```
G91 ;Relative positioning
G1 E-2 F2700 ;Retract a bit
G1 E-2 Z0.2 F2400 ;Retract and raise Z
G1 X5 Y5 F3000 ;Wipe out
G1 Z10 ;Raise Z more
G90 ;Absolute positioning

G1 X0 Y{machine_depth} ;Present print
M106 S0 ;Turn-off fan
M104 S0 ;Turn-off hotend
M140 S0 ;Turn-off bed

M84 X Y E ;Disable all steppers but Z
```

## Printer settings

| Setting                         | Value       |
| ---                             | ---         |
| X (Width)                       | 220.0 mm    |
| Y (Width)                       | 220.0 mm    |
| Z (Height)                      | 250.0 mm    |
| Build plate shape               | Rectangular |
| Origin at center                | false       |
| Heated bed                      | true        |
| Heated build volume             | false       |
| G-code flavor                   | Marlin      |
| X min                           | -26 mm      |
| Y min                           | -32 mm      |
| X max                           | 32 mm       |
| Y max                           | 34 mm       |
| Gantry height                   | 25.0 mm     |
| Number of Extruders             | 1           |
| Apply Extruder offsets to GCode | true        |

## Extruder settings

| Setting                      | Value   |
| ---                          | ---     |
| Compatible material diameter | 1.75 mm |
| Nozzle offset X              | 0.0 mm  |
| Nozzle offset Y              | 0.0 mm  |
| Cooling fan number           | 0       |
